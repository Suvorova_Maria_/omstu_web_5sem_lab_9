import { configureStore } from '@reduxjs/toolkit';
import { render, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { AppRoutes } from '../../common/constants';
import { Comment, Post as PostType, User } from '../../common/types';
import { rootReducer } from '../../store/slices';
import { Post } from './Post';

const mockPost: PostType = { id: 1, title: 'Hello', body: 'World', userId: 1 };
const mockUser: User = {
  id: 1,
  name: 'John Doe',
  email: 'example@com.com',
  phone: '+79032345322',
  username: 'johndoe',
};
const mockCommentAuthor: User = {
  id: 1,
  name: 'Ivan Ivanod',
  email: 'ivan@com.com',
  phone: '+79032345366',
  username: 'ivan_ivan',
};
const mockComments: Comment[] = [
  {
    id: 1,
    body: 'Mock comment #1',
    email: mockCommentAuthor.email,
    name: mockCommentAuthor.name,
    postId: mockPost.id,
  },
  {
    id: 2,
    body: 'Mock comment #2',
    email: mockCommentAuthor.email,
    name: mockCommentAuthor.name,
    postId: mockPost.id,
  },
];

jest.mock('../../api', () => ({
  getSinglePost: async () => mockPost,
  getSingleUser: async () => mockUser,
  getPostComments: () => mockComments,
}));

describe('Тесты для Post', () => {
  const store = configureStore({ reducer: rootReducer });

  it('Я вижу тело и заголовок статьи', async () => {
    const { queryByText } = render(
      <Provider store={store}>
        <BrowserRouter>
          <Routes>
            <Route path={`${AppRoutes.POSTS_BASE}/:id`} element={<Post />} />
            <Route
              path="*"
              element={<Navigate to={AppRoutes.POST(mockPost.id)} />}
            />
          </Routes>
        </BrowserRouter>
      </Provider>
    );

    await waitFor(async () => {
      const title = queryByText(mockPost.title, { exact: false });
      expect(title).toBeTruthy();

      const body = queryByText(mockPost.body, { exact: false });
      expect(body).toBeTruthy();
    });
  });

  it('Я вижу ссылку на профиль автора статьи', async () => {
    const { queryByText } = render(
      <Provider store={store}>
        <BrowserRouter>
          <Routes>
            <Route path={`${AppRoutes.POSTS_BASE}/:id`} element={<Post />} />
            <Route
              path="*"
              element={<Navigate to={AppRoutes.POST(mockPost.id)} />}
            />
          </Routes>
        </BrowserRouter>
      </Provider>
    );

    await waitFor(async () => {
      const user = queryByText(mockUser.name, { exact: false });
      expect(user).toBeTruthy();
      expect(user?.tagName).toBe('A');
    });
  });

  it('Я вижу комментарии к статье', async () => {
    const { queryByText } = render(
      <Provider store={store}>
        <BrowserRouter>
          <Routes>
            <Route path={`${AppRoutes.POSTS_BASE}/:id`} element={<Post />} />
            <Route
              path="*"
              element={<Navigate to={AppRoutes.POST(mockPost.id)} />}
            />
          </Routes>
        </BrowserRouter>
      </Provider>
    );

    await waitFor(async () => {
      const firstComment = queryByText(mockComments[0].body, { exact: false });
      expect(firstComment).toBeTruthy();

      const secondComment = queryByText(mockComments[1].body, { exact: false });
      expect(secondComment).toBeTruthy();
    });
  });
});
