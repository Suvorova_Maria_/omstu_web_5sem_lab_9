import { configureStore } from '@reduxjs/toolkit';
import { render, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { AppRoutes } from '../../common/constants';
import { User } from '../../common/types';
import { rootReducer } from '../../store/slices';
import { Profile } from './Profile';

const mockUser: User = {
  id: 1,
  name: 'John Doe',
  email: 'example@com.com',
  phone: '+79032345322',
  username: 'johndoe',
};

jest.mock('../../api', () => ({
  getSingleUser: async () => mockUser,
}));

describe('Тесты для Profile', () => {
  const store = configureStore({ reducer: rootReducer });

  it('Я вижу имя, почту и телефон человека', async () => {
    const { queryByText } = render(
      <Provider store={store}>
        <BrowserRouter>
          <Routes>
            <Route
              path={`${AppRoutes.PROFILES_BASE}/:id`}
              element={<Profile />}
            />
            <Route
              path="*"
              element={<Navigate to={AppRoutes.PROFILE(mockUser.id)} />}
            />
          </Routes>
        </BrowserRouter>
      </Provider>
    );

    await waitFor(
      async () => {
        const name = queryByText(mockUser.name, { exact: false });
        expect(name).toBeTruthy();
        const phone = queryByText(mockUser.phone, { exact: false });
        expect(phone).toBeTruthy();
        const email = queryByText(mockUser.email, { exact: false });
        expect(email).toBeTruthy();
      },
      { timeout: 2000 }
    );
  });
});
