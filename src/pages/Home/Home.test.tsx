import { configureStore } from '@reduxjs/toolkit';
import { render, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { rootReducer } from '../../store/slices';
import { Home } from './Home';

const mockPost = { id: 1, title: 'Hello', body: 'World' };

jest.mock('../../api', () => ({
  getManyPosts: async () => {
    return [mockPost];
  },
}));

describe('Тесты для Home', () => {
  const store = configureStore({ reducer: rootReducer });

  it('Я вижу тело и заголовок поста с API', async () => {
    const { queryByText } = render(
      <Provider store={store}>
        <Home />
      </Provider>
    );

    await waitFor(async () => {
      const title = queryByText(mockPost.title, { exact: false });
      expect(title).toBeTruthy();

      const body = queryByText(mockPost.body, { exact: false });
      expect(body).toBeTruthy();
    });
  });

  it('Я вижу, что заголовок статьи является гиперссылкой', async () => {
    const { queryByText } = render(
      <Provider store={store}>
        <Home />
      </Provider>
    );

    await waitFor(async () => {
      const title = queryByText(mockPost.title, { exact: false });
      expect(title).toBeTruthy();
      expect(title?.tagName).toBe('A');
    });
  });
});
