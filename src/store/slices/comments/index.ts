import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../..';
import { getPostComments } from '../../../api';
import { Comment, FetchStatus } from '../../../common/types';

const initialState = {
  postsCommentsLoadingMap: {} as Record<number, FetchStatus>,
  comments: [] as Comment[],
};

const fetchCommentsByPostId = createAsyncThunk<Comment[], number>(
  'comments/fetchByPostId',
  async (postId, _) => {
    const comments = await getPostComments(postId);

    return comments;
  }
);

/*
createSlice - принимает начальное состояние, объект 
функций-редукторов и «имя среза» и автоматически 
генерирует создателей действий и типы действий,
которые соответствуют редукторам и состоянию.
*/
export const slice = createSlice({
  name: 'comments', //Строковое имя для состояния.
  initialState, //Значение начального состояния для этого фрагмента состояния
  reducers: {}, //Объект, содержащий функции Redux-редуктора 
  extraReducers: (builder) => {
    builder.addCase(fetchCommentsByPostId.pending, (state, { meta }) => {
      state.postsCommentsLoadingMap[meta.arg] = 'pending';
    });
    builder.addCase(
      fetchCommentsByPostId.fulfilled,
      (state, { meta, payload }) => {
        state.postsCommentsLoadingMap[meta.arg] = 'fulfilled';
        state.comments = state.comments.concat(
          payload.filter((c) => !state.comments.find((tc) => tc.id === c.id))
        );
      }
    );
    builder.addCase(fetchCommentsByPostId.rejected, (state, { meta }) => {
      state.postsCommentsLoadingMap[meta.arg] = 'rejected';
    });
  },
});

const selectPostComments = (postId: number) => (state: RootState) =>
  state.comments.comments.filter((comment) => comment.postId === postId);

export const actions = { ...slice.actions, fetchCommentsByPostId };
export const reducer = slice.reducer;
export const selectors = { selectPostComments };
